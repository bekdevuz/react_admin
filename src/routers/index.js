// Containers
import AuthContainer from "@containers/Auth_container/index";
//pages
import Dashboard from "@pages/dashboard/index";
import Orders from "@pages/orders";
import Settings from "@pages/settings";
import Clients from "@pages/users";
import History from "@pages/history";
import Marketing from "@pages/marketing";

import P404 from "../components/exceptions/P404";
import P403 from "../components/exceptions/P403";
import P502 from "../components/exceptions/P502";
export const routes = [
  {
    path: "/",
    title: "dashboard",
    hideChildren: false,
    component: Dashboard,
    children: [],
  },
  {
    path: "/login",
    title: "login",
    hideChildren: false,
    component: AuthContainer,
    children: [],
  },

  {
    path: "*",
    title: "404 Error",
    hideChildren: false,
    component: P404,
    children: [],
  },
  {
    path: "/403",
    title: "403 Error",
    hideChildren: false,
    component: P403,
    children: [],
  },
  {
    path: "/502",
    title: "502 Error",
    hideChildren: false,
    component: P502,
    children: [],
  },
  {
    path: "/store",
    title: "Store",
    hideChildren: false,
    component: Dashboard,
    children: [],
  },
  {
    path: "/orders",
    title: "Orders",
    hideChildren: false,
    component: Orders,
    children: [],
  },
  {
    path: "/star",
    title: "Star",
    hideChildren: false,
    component: Dashboard,
    children: [],
  },

  {
    path: "/assignment",
    title: "Assignment",
    hideChildren: false,
    component: Dashboard,
    children: [],
  },
  {
    path: "/history",
    title: "History",
    hideChildren: false,
    component: History,
    children: [],
  },
  {
    path: "/settings",
    title: "Settings",
    hideChildren: false,
    component: Settings,
    children: [],
  },
  {
    path: "/clients",
    title: "Clients",
    hideChildren: false,
    component: Clients,
    children: [],
  },
  {
    path: "/marketing",
    title: "Marketing",
    hideChildren: false,
    component: Marketing,
    children: [],
  },
];
