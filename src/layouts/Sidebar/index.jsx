import LogoImg from "../../assets/images/logo.png";
import ProfilePic from "../../assets/images/Profile.png";
import {
  Analytics,
  ShoppingBasket,
  Assignment,
  Settings,
  PeopleSharp,
  History,
  Star,
  Store,
  Notifications,
} from "@mui/icons-material";
//END
import { Link } from "react-router-dom";

export default function App() {
  return (
    <div className="bg-white  z-10 flex fixed">
      <div className="h-screen  flex flex-col justify-between  items-center  border-r">
        <div className="mt-5 ml-5 ">
          <Link to="/">
            <img src={LogoImg} alt="delver-logo" />
          </Link>
          <div className="mt-5 cursor-pointer">
            <Link to="/">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-1 hover:bg-blue-200">
                <Analytics sx={{ color: "#6E8BB7", fontSize: "30px" }} />
              </div>
            </Link>{" "}
            <Link to="/clients">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-2 hover:bg-blue-200">
                <PeopleSharp sx={{ color: "#6E8BB7" }} />
              </div>
            </Link>
            <Link to="/orders">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-2 hover:bg-blue-200">
                <ShoppingBasket sx={{ color: "#6E8BB7" }} />
              </div>
            </Link>
            <Link to="/assignment">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-2 hover:bg-blue-200">
                <Assignment sx={{ color: "#6E8BB7" }} />
              </div>
            </Link>
            <Link to="/store">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-2 hover:bg-blue-200">
                <Store sx={{ color: "#6E8BB7" }} />
              </div>
            </Link>
            <Link to="/star">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-2 hover:bg-blue-200">
                <Star sx={{ color: "#6E8BB7" }} />
              </div>
            </Link>
            <Link to="/history">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-2 hover:bg-blue-200">
                <History sx={{ color: "#6E8BB7" }} />
              </div>
            </Link>
            <Link to="/settings">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-2 hover:bg-blue-200">
                <Settings sx={{ color: "#6E8BB7" }} />
              </div>
            </Link>
          </div>
        </div>

        <div className="mb-4  flex-col align-center flex justify-center">
          <Notifications sx={{ color: "#6E8BB7", fontSize: "35px" }} />
          <img className="mt-4" src={ProfilePic} alt="profile-img" />
        </div>
      </div>
    </div>
  );
}
