export default function App({ title }) {
  return (
    <section className="ml-20  h-12 flex  relative border-b">
      <h1 className="mt-2 ml-8 text-3xl font-semibold">{title}</h1>
    </section>
  );
}
