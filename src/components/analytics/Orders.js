import React from "react";
import Green from "../../assets/images/greenDiagram.png";
import Purple from "../../assets/images/PurpleDiagram.png";
import { Line } from "react-chartjs-2";

// import Chart from "chart.js";

export default function Orders() {
  return (
    <div className="mt-6 ml-7 w-full sm:ml-0 p  ">
      <div className=" flex   sm:flex-wrap md:flex-wra ">
        <div className="sm:w-96 justify-between mr-8 bg-white w-1/2 flex border rounded-lg p-4 items-center ">
          <img src={Green} alt="diagram" />
          <div className=" mr-6 ml-8 ">
            <h1 className="  text-gray-500">Top Orders</h1>
            <h1 className=" font-semibold text-5xl text-greensecondary">75%</h1>
          </div>
        </div>
        <div className="sm:w-96  justify-between  sm:mt-4 bg-white w-1/2 flex border rounded-lg p-4 items-center  ">
          <img src={Purple} alt="diagram" />
          <div className=" sm:inline mr-6 ml-8    ">
            <h1 className="  text-gray-500">Top Orders</h1>
            <h1 className="font-semibold text-5xl text-purple">75%</h1>
          </div>
        </div>
      </div>

      <MonthlyStat />
    </div>
  );
}

function MonthlyStat() {
  return (
    <div className="border bg-white rounded-lg mt-6  ">
      <div className="flex justify-between ">
        <h1 className="m-4 text-xl font-semibold text-gray-500">
          Ежемесячная статистика
        </h1>
        <div></div>
      </div>
      <div className="ml-8 mr-8 mb-6">
        <BarChart />
      </div>

      <div className="border-t ">
        <div class="flex items-center justify-center">
          <div class="inline-flex" role="group">
            <button
              type="button"
              className="
     
        px-6
        py-2
        border-b-4 border-skyblue
        text-skyblue
        font-medium
        text-xs
        leading-tight
        uppercase
        hover:bg-black hover:bg-opacity-5
        focus:outline-none focus:ring-0
        transition
        duration-150
        ease-in-out
      "
            >
              Неделя
            </button>{" "}
            <button
              type="button"
              className="
     
        px-6
        py-2
     
        text-lightgray
        font-medium
        text-xs
        leading-tight
        uppercase
        hover:bg-black hover:bg-opacity-5
        focus:outline-none focus:ring-0
        transition
        duration-150
        ease-in-out
      "
            >
              Месяц
            </button>{" "}
            <button
              type="button"
              className="
     
        px-6
        py-2
      
        text-lightgray
        font-medium
        text-xs
        leading-tight
        uppercase
        hover:bg-black hover:bg-opacity-5
        focus:outline-none focus:ring-0
        transition
        duration-150
        ease-in-out
      "
            >
              Год
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

const BarChart = () => {
  const removerTitle = (tooltipItems) => {
    return "";
  };
  return (
    <div>
      <Line
        type="bar"
        data={{
          datasets: [
            {
              label: "Июль",
              data: [
                "200",
                "150",
                "450",
                "550",
                "520",
                "280",
                "700",
                "900",
                "1000",
              ],
              backgroundColor: "rgba(14, 115, 246, 0.4)",
              borderColor: "#0E73F6",
              borderWidth: 2,
            },
          ],
          labels: [
            "Понедельник",
            "Вторник",
            "Среда",
            "Четверг",
            " Пятница",
            "Суббота ",
            "Воскресенье",
          ],
        }}
        height={400}
        width={580}
        options={{
          maintainAspectRatio: false,
          responsive: true,
          tooltips: {
            backgroundColor: "#fff",
            bodyFontColor: "#0E73F6",
            titleAlign: "center",
            titleFontColor: "#000",
            callbacks: { title: removerTitle },
          },

          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: true,
                },
              },
            ],
          },
        }}
      />
    </div>
  );
};
