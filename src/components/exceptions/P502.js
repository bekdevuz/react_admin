import React from "react";
import Pic502 from "../../assets/images/502.png";
import { Link } from "react-router-dom";
export default function P502() {
  return (
    <div className="flex mt-60 justify-center  flex-wrap">
      <div>
        <h1 className=" text-blue-500  text-9xl font-semibold ">502</h1>
        <h2 className="font-semibold text-4xl mt-10 ">Server Error</h2>
        <p className="mt-10">Sorry, the page you visited does not exist.</p>
        <Link to="/">
          {" "}
          <button className="bg-blue-500 p-2 text-white rounded mt-14">
            Вернутся в главную
          </button>
        </Link>
      </div>
      <div className="ml-20">
        <img src={Pic502} alt="502-img" />
      </div>
    </div>
  );
}
