import { TableRows, Add, Edit, LocationOn } from "@mui/icons-material";

export default function App({ title }) {
  return (
    <section className="ml-20  h-12 flex justify-between relative border-b">
      <h1 className="mt-2 ml-8 text-3xl font-semibold">{title}</h1>
      <div class=" md:hidden  grid grid-cols-4  divide-x mr-6">
        <div className="flex items-center ">
          <TableRows sx={{ color: "#6E8BB7" }} />
          <p className="mx-2 text-lightgray">Таблица</p>
        </div>
        <div className="flex items-center ">
          <LocationOn className="ml-4" sx={{ color: "#6E8BB7" }} />
          <p className="mx-2 text-lightgray">Карта</p>
        </div>
        <div className="flex items-center ">
          <Edit className="ml-4" sx={{ color: "#4094F7" }} />
          <p className="mx-2 text-skyblue">Дейтсвия</p>
        </div>
        <div className="flex items-center ">
          <Add className="ml-4" sx={{ color: "#1AC19D" }} />
          <p className="mx-2 text-greensecondary">Добавить</p>
        </div>
      </div>
    </section>
  );
}
