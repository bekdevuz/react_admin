import LogoImg from "../../assets/images/logo.png";
import ProfilePic from "../../assets/images/Profile.png";
import {
  Analytics,
  LocalGroceryStore,
  PeopleSharp,
  MyLocation,
  Notifications,
  Settings,
} from "@mui/icons-material";
//END
import { Link } from "react-router-dom";

export default function SecondSidebar({ children }) {
  return (
    <div className=" bg-white flex fixed">
      <div className="h-screen flex flex-col justify-between  items-center  border-r">
        <div className="mt-5 ml-5 ">
          <Link to="/">
            <img src={LogoImg} alt="delver-logo" />
          </Link>
          <div className="mt-5 cursor-pointer">
            <Link to="/orders">
              <div className="w-10 h-10 flex bg-blue-500 mr-5 rounded p-2 hover:bg-blue-200">
                <LocalGroceryStore sx={{ color: "#fff" }} />
              </div>
            </Link>
            <Link to="/clients">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-2 hover:bg-blue-200">
                <PeopleSharp sx={{ color: "#6E8BB7" }} />
              </div>
            </Link>
            <Link to="/marketing">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-1 hover:bg-blue-200">
                <MyLocation sx={{ color: "#6E8BB7", fontSize: "30px" }} />
              </div>
            </Link>
            <Link to="/">
              <div className="w-10 h-10 flex bg-gray-100 mr-5 rounded p-1 hover:bg-blue-200">
                <Analytics sx={{ color: "#6E8BB7", fontSize: "30px" }} />
              </div>
            </Link>
          </div>
        </div>

        <div className="mb-4  flex-col  flex justify-center">
          <Link to="/settings">
            <div className="flex justify-center rounded mb-4 ">
              <Settings sx={{ color: "#6E8BB7", fontSize: "30px" }} />
            </div>
          </Link>
          <Notifications sx={{ color: "#6E8BB7", fontSize: "37px" }} />
          <img className="mt-4" src={ProfilePic} alt="profile-img" />
        </div>
      </div>
    </div>
  );
}
