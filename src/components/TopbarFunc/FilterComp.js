import React from "react";
import {
  ChevronRightRounded,
  ChevronLeftRounded,
  FileDownloadOutlined,
  FilterAlt,
  Search,
  TableChartRounded,
} from "@mui/icons-material";
export default function FilterComp() {
  return (
    <section className="md:flex-wrap ml-20 md:h-full bg-gray-100 h-12 flex  justify-between relative border-b">
      <div className="flex items-center ml-6 md:mb-4">
        <Search className="absolute mx-2" sx={{ color: "#B0BABF" }} />
        <input
          className=" py-1  mr-2 pl-8 pr-44 appearance-none border-2 rounded w-full  text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="username"
          type="text"
          placeholder="Поиск"
        />
      </div>

      <div className="flex  items-center mx-10 md:mb-4">
        <div class="mr-5  inline-block relative w-32">
          <div class="pointer-events-none absolute inset-y-0  left-0 flex items-center px-2 text-gray-700">
            <ChevronLeftRounded color="primary" />
          </div>
          <select class=" block appearance-none w-full bg-white border   hover:border-skyblue px-8 py-1 pr-4 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
            <option>Сегодня</option>
            <option>Вчера</option>
            <option>Завтра</option>
          </select>
          <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <ChevronRightRounded color="primary" />
          </div>
        </div>
        <div className="flex ml-6">
          <TableChartRounded />
          <button className="ml-2 ">Столбцы</button>
        </div>
        <div className="flex ml-6">
          <FilterAlt />
          <button className="ml-2">Фильтр</button>
        </div>
        <div className="flex ml-6">
          <FileDownloadOutlined />
          <button className="ml-1">Скачать</button>
        </div>
      </div>
    </section>
  );
}
