import Wrapper from "@layouts/Wrapper";
// import Orders from "../../components/Orders/Orders";
import SecondNav from "../../components/navbar/SecondNav";
import SecondSidebar from "../../components/sidebars/SecondSidebar";
import FilterComp from "../../components/TopbarFunc/FilterComp";
export default function App() {
  return (
    <div>
      <SecondSidebar />
      <SecondNav title="Orders"></SecondNav> <FilterComp />
      <Wrapper />
      {/* <Orders /> */}
    </div>
  );
}
