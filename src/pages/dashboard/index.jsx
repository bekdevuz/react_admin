import Wrapper from "@layouts/Wrapper";
import AnalyticsCom from "../../components/analytics/AnalyticsCom";
import Navbar from "@layouts/Navbar";
import Sidebar from "@layouts/Sidebar";
export default function App() {
  return (
    <div>
      <Sidebar />
      <Navbar title="Analytics"></Navbar>
      <Wrapper />
      <AnalyticsCom />
    </div>
  );
}
