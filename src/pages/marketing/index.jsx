import Wrapper from "@layouts/Wrapper";
import Navbar from "@layouts/Navbar";
import Sidebar from "@layouts/Sidebar";
export default function App() {
  return (
    <div>
      <Sidebar />
      <Navbar title="Marketing"></Navbar>
      <Wrapper />
    </div>
  );
}
